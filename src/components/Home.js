import react from 'react';
import styled from 'styled-components';
import iphone  from '../images/iphone.png';
import cart_icon from '../images/cart_icon.png';

const ImgContainer = styled.img`
width : 100px;

`;
const CartWrapper = styled.div`
width : 500px;
height:120px;
border: 1px solid gray;
margin-top:2rem;
display:flex;
padding:10px;
justify-content:center;
`;

const StyledButton= styled.button`
background-color : red;
padding:10px;
border: 0px;
border-radius:5px;
color:white;
`;

const StyledItem = styled.div`
float:left;
width:120px;
padding:20px;
`;

const StyledCart =styled.div`

`;

const StyledCount =styled.span`
position:absolute;
right:10px;
top:10px;
z-index:1;
background-color:red;
padding:7px;
width:20px;
height:20px;
color:white;
border-radius:50px;
text-align:center;
`;

function Home(props){

    console.log("props",props.data);

return (
    <div>
    
    <h3 style={{display:"flex", justifyContent:"center"}}>REDUX TESTING</h3>
<StyledCart>
<StyledCount>{props.data.length}</StyledCount>

    <img style={{width:"80px",position:"absolute",right:"1%",top:"1%"}} src={cart_icon}/></StyledCart>


    <div style={{display:"flex", justifyContent:"center"}}>

        
    

    <CartWrapper>

    <div className="img_wrapper item">
    <StyledItem>
    <ImgContainer src={iphone} />
    </StyledItem>
    </div>

    <div className="text_wrapper item">
        <StyledItem>
        <span>I-Phone</span>
        <span>Price : $1000</span>
        </StyledItem>
    </div>

   

    <div className="btn_wrapper item">
    <StyledItem>
        <StyledButton onClick={()=>props.addToCartHandler({price:1000,name:' Redme 8 pro'})}>Add to cart</StyledButton>
        </StyledItem>
    </div>


    </CartWrapper>
    </div>
</div>
)
}

export default Home