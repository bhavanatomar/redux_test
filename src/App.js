import './App.css';
import HomeContainer from './container/HomeContainer';

function App() {
  return (
    <div>
    
      {/* <Home/>  we can pass it through the container so we can connect it with redux or map the componet and redux*/} 
    <HomeContainer/>
    </div>
  );
}

export default App;
