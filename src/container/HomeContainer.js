import {connect} from 'react-redux';
import { addToCart } from '../services/Action/action';

 import Home from '../components/Home';
// import cardItems from '../services/Reducer/reducer';

 const mapStateToProps  =state=>({
            data:state.cartItems

 })


 const mapDispatchToProps =dispatch=>({

    addToCartHandler:data=>dispatch(addToCart(data))

 })

  export default connect(mapStateToProps,mapDispatchToProps)(Home)