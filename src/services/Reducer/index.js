// Root Reducer
// Here we can combine all the reducer used in redux 

import {combineReducers} from 'redux';
import cartItems from './reducer';


export default combineReducers({
    cartItems
})