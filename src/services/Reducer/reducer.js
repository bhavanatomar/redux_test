// 1.  here we decare initial state (what should be the value of store initially ,, it may be blank, array, or other data.)
// 2. here we can use the switch cases based on defined actions .

import { ADD_TO_CART } from '../constants';


const initialState={
    cardData:[]
}

export default function cardItems(state=[], action)   // we never need to import actions  bcoz they called self.

{
    switch (action.type){
      case ADD_TO_CART:
          return[
              ...state,
     { cardData:action.data}
          ]
    // break;
    default:
        return state
}

} 